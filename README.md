# Stupid Snake

A snake game for terminal; stupid because simple (though it might get smarter later).

Many Unix (or even Linux) specific console shenanigans used. I did not want to utilize ncurses, though I know it could be simpler and more portable to do so.

### Controls

Arrow keys - movement.
If the same key is pressed twice (in other words, press right when snake is going right) - speed boost.
Exit - Ctrl+C, or just slam into something.

### Speed boost note

Since this is a terminal game, there is no way to detect key up/down events. Thus, maximum boost is determined by your terminal's key repeat rate.
If you find it insufficient, please alter the key repeat rate.
